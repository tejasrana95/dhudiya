<?php global $base_url; ?>
<?php

$theme_path = drupal_get_path('theme', $GLOBALS['theme']);
include_once($theme_path . '/templates/include/sidebar_menu.tpl.php');

if (!empty($content['field_sidebar_widget'])) {
	$field_sidebar = count($content['body']['#object']->field_sidebar_widget['und']);
	echo '<div class="sidebar_blocks sidebar_blocks_' . $field_sidebar . '" id="sidebar_blocks">';
	print render($content['field_sidebar_widget']);
	echo '</div>';
} 
?>