<?php global $base_url; ?>

<div class="sd-logo-box"><a href="<?php print $base_url; ?>" title="<?php print t('Home'); ?>"> <img src="<?php echo $base_url . '/' . $theme_path ?>/images/logo-1.png" alt="<?php print t('Home'); ?>" /></a></div>

<?php
$theme_path = drupal_get_path('theme', $GLOBALS['theme']);
include_once($theme_path . '/templates/include/sidebar_menu.tpl.php');


if (!empty($content['field_sidebar_widget'])) {
    $field_sidebar = count($content['body']['#object']->field_sidebar_widget['und']);
    echo '<div class="sidebar_blocks sidebar_blocks_' . $field_sidebar . '" id="sidebar_blocks">';

    print render($content['field_sidebar_widget']);
    echo '</div>';
} else {
    $currentUrl = drupal_get_path_alias(current_path());
    $parent = explode('/', $currentUrl);
if(count($parent)>2){
	unset($parent[count($parent)-1]);
		$parent=implode('/',$parent );
	}else{
	$parent=$parent[0];
	}
    $node_path = explode('/', drupal_get_normal_path($parent));
	

	if(isset($node_path[1])){
	
    $node = node_load($node_path[1]);
	
	}else{
	 //$node = node_load($node_path[0]);
	}
	if(isset($node)){ 
	
	
    $content = node_view($node);

    if (!empty($content['field_sidebar_widget'])) {
        $field_sidebar = count($content['body']['#object']->field_sidebar_widget['und']);
        echo '<div class="sidebar_blocks_' . $field_sidebar . '" id="sidebar_blocks">';

        print render($content['field_sidebar_widget']);
        echo '</div>';
    }
	}
}
?>