<?php
global $base_url;
$theme_path = drupal_get_path('theme', $GLOBALS['theme']);
global $site_name;
?>
<footer class="page-section bg-gray-lighter footer pb-60">
    <div class="container">
        <!-- Footer Logo -->
        <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.5s">
            <a href="/"><img src="<?php echo $base_url . '/' . $theme_path; ?>/images/logo-footer.png" alt="<?php echo $site_name ?>" title="<?php echo $site_name ?>" /></a>
        </div>
        <!-- End Footer Logo -->
        <?php
        $block = module_invoke('infi_social_links', 'block_view', 'infi_social_footer');
        print render($block['content']);
        ?>
        <!-- End Social Links --> 
        <?php if ($page['footer_before_logo']): ?>
            <?php print render($page['footer_before_logo']); ?>
        <?php endif; ?>
        <!-- Footer Text -->
        <div class="footer-text">
            <!-- Copyright -->
            <div class="footer-copy font-alt">
                <?php
                $block = module_invoke('block', 'block_view', '1');
                print render($block['content']);
                ?>
            </div>
            <!-- End Copyright -->
            <div class="footer-made">
                <?php
                $block = module_invoke('block', 'block_view', '2');
                print render($block['content']);
                ?>
            </div>
        </div>
        <!-- End Footer Text --> 
        <?php if ($page['footer_after_links']): ?>
            <?php print render($page['footer_after_links']); ?>
        <?php endif; ?>
    </div>
    <?php if ($page['footer_full_width']): ?>
        <?php print render($page['footer_full_width']); ?>
    <?php endif; ?>
    <!-- Top Link -->
    <div class="local-scroll">
        <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
    </div>
    <!-- End Top Link -->
    <?php
    if (!empty($tabs) && $user->uid != "") {
        ?>
        <div id="user-shortcut-tab">
            <div id="cd-nav" class="is-fixed">
                <a href="javascript:void('infi')" class="cd-nav-trigger"><span><div class="display_none">Quick Action</div></span></a>
                <nav id="cd-main-nav">
                    <?php print render($tabs); ?>
                </nav>
            </div>
        </div>
        <?php
    }
    ?>