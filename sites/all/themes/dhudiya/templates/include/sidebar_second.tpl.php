<?php
        //print render($page['inner_sidebar']); 
        $right_field = field_view_field('node', $node, 'field_sidebar_menu');
		$hasValue=false;
		if (!empty($right_field)) {
           $hasValue=true;
        } else {
            $currentUrl = drupal_get_path_alias(current_path());
            $parent = explode('/', $currentUrl);
            $node_path = explode('/', drupal_get_normal_path($parent[0]));
            $node = node_load($node_path[1]);
            $content = node_view($node);
            $right_field = field_view_field('node', $node, 'field_sidebar_menu');
			if (!empty($right_field)) {
				$hasValue=true;
			}
        }
		if(isset($hasValue) && $hasValue==true):
		?>
<div class="sidemenu_div" id="">
    <aside class="field-items" id="sidemenu_div_menu" role="complementary">
        <?php
       if (!empty($right_field)) {
            print render($right_field);
        } else {
            $currentUrl = drupal_get_path_alias(current_path());
            $parent = explode('/', $currentUrl);
            $node_path = explode('/', drupal_get_normal_path($parent[0]));
            $node = node_load($node_path[1]);
            $content = node_view($node);
            $right_field = field_view_field('node', $node, 'field_sidebar_menu');
            print render($right_field);
        }
        ?>
    </aside>  <!-- /#sidebar-second -->

</div>
<?php endif; ?>