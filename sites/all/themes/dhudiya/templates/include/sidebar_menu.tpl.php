<div class="sidemenu_div" id="sidemenu_div">
    <aside class="field-items" id="sidemenu_div_menu" role="complementary">
        <?php
        $right_field = field_view_field('node', $node, 'field_sidebar_menu', array('label' => 'hidden'));
        $right_field_type=$right_field['#object']->field_sidebar_menu['und'][0]['moddelta'];     

        if($right_field_type=="menu_block:4"){
           $menu_item = db_select('menu_links', 'm')
           ->fields('m', array('depth', 'menu_name', 'has_children'))
           ->condition('link_path', $_GET['q'], '=')
           ->execute()
           ->fetchAssoc();
            if($menu_item['has_children']==0){
                $active_trail = menu_get_active_trail();
                $parent = $active_trail[count($active_trail)-3];
               // debugs($parent);
                if($parent['title']=="Home"){
                     $parent = $active_trail[count($active_trail)-2];
                }
            }else{
                $active_trail = menu_get_active_trail();
                $parent = $active_trail[count($active_trail)-2];
            }
            $config=array('menu_name'=>$parent['menu_name'],'parent_mlid'=>$parent['mlid'],'level'=>1,'depth'=>5);
            $menu=  menu_tree_build($config);
           ?>
           <div class="field field-name-field-sidebar-menu field-type-blockreference field-label-hidden">
            <div class="field-items">
                <div class="field-item even">
                    <div id="block-menu-block-5--2" class="block-menu-block contextual-links-region visible animated fadeIn">
                        <h2><a href="/<?php echo drupal_get_path_alias($parent['link_path']) ?>" title="<?php echo  $parent['link_title']; ?>"><?php echo  $parent['link_title']; ?></a></h2>
                        <div class="content">
                            <div class="menu-block-wrapper menu-block-5 menu-name-main-menu parent-mlid-0 menu-level-1">
                            <?php print drupal_render($menu['content']['#content']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php    
        }elseif (!empty($right_field)) {
            print render($right_field);
        } else {?>
        <?php
      
           $menu_item = db_select('menu_links', 'm')
           ->fields('m', array('depth', 'menu_name', 'has_children'))
           ->condition('link_path', $_GET['q'], '=')
           ->execute()
           ->fetchAssoc();
            if($menu_item['has_children']==0){
                $active_trail = menu_get_active_trail();
                $parent = $active_trail[count($active_trail)-2];
               // debugs($parent);
                if($parent['title']=="Home"){
                     $parent = $active_trail[count($active_trail)-2];
                }
            }else{
                $active_trail = menu_get_active_trail();
                $parent = $active_trail[count($active_trail)-2];
            }
            $config=array('menu_name'=>$parent['menu_name'],'parent_mlid'=>$parent['mlid'],'level'=>1,'depth'=>5);
            $menu=  menu_tree_build($config);
           ?>
       <div class="field field-name-field-sidebar-menu field-type-blockreference field-label-hidden">
            <div class="field-items">
                <div class="field-item even">
                    <div id="block-menu-block-5--2" class="block-menu-block contextual-links-region visible animated fadeIn">
                        <h2><a href="/<?php echo drupal_get_path_alias($parent['link_path']) ?>" title="<?php echo  $parent['link_title']; ?>"><?php echo  $parent['link_title']; ?></a></h2>
                        <div class="content">
                            <div class="menu-block-wrapper menu-block-5 menu-name-main-menu parent-mlid-0 menu-level-1">
                            <?php print drupal_render($menu['content']['#content']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
    }
        ?>
    </aside>  <!-- /#sidebar-second -->

</div>