<?php
//****************************************************//
/*
  Theme Template for functionality for Academic Programs main menu's inner pages
 */
//****************************************************//
/**
 * @file_field_delete_file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
global $base_url;
$theme_path = drupal_get_path('theme', $GLOBALS['theme']);
$currentUrl = drupal_get_path_alias(current_path());
$currentUrl = $base_url . '/' . $currentUrl;

$parentUrl = explode('/', drupal_get_path_alias(current_path()));
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

   <?php  $field_youtube_embed_source = field_get_items('node', $node, 'field_youtube_embed_source'); 
   if(isset($field_youtube_embed_source)){ ?>
   <div class="work-full-media mt-0 white-shadow grey-bg" id="trailer">
    <div class="container">
        <?php
        echo $field_youtube_embed_source[0]['value'];
        ?>

    </div>
    <div class="clearfix">&nbsp;</div>
</div>
<?php  
}
?>


<div class="video-information">

    <div class="row">
        <div class="col-md-9 col-sm-12">
            <h1 class="uppercase"><?php print $title; ?></h1>
            <div class="video-info">

                <div class="synopsis">
                    <?php print $body[0]['value']; ?>
                </div>
                <div class="video-categories">
                    <h5 class="font-alt uppercase"><strong>Category</strong></h5>
                    <?php  $field_video_categories = field_get_items('node', $node, 'field_video_category'); 
                    $genres=array();
                    $category_tid=array();
                    foreach($field_video_categories as $field_video_category){
                        $genres[$field_video_category['taxonomy_term']->tid]=
                                '<a href="'.$base_url.'/videos/?category='.$field_video_category['taxonomy_term']->tid.'">'.$field_video_category['taxonomy_term']->name.'</a>';
                    }
                    print implode('  ', $genres);
                    ?>
                </div>
                <?php
                $field_link = field_get_items('node', $node, 'field_link');
                if(isset($field_link[0])){
                    ?>
                     <h5 class="font-alt uppercase"><strong><?php  echo '<a href="'.$field_link['0']['url'].'" id="" class="" '.target_gen($field_link['0']['attributes']).'>'.$field_link['0']['title'].'</a> '; ?></strong></h5>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-12">
                <h3 class="uppercase">Related Videos</h3>
                <div class="related-videos">
                    <?php 
                    $filter_result = views_get_view('video');
                    $filter_result->set_display('related_videos');
                    $filter_result->set_arguments(array(implode('+', array_keys($genres))));
                    $filter_result->pre_execute();

                    $preview = $filter_result->preview('related_videos');
                    print $preview;
                    ?>
                </div>
            </div>
        </div>
    </div>                    
</div>