<main class="main-wrapper">

    <?php
    $theme_path = drupal_get_path('theme', $GLOBALS['theme']);
    include_once $theme_path . '/templates/include/header.tpl.php';
    ?>
    <main class="main-cpart">
        <div class="container">
            <div class="row">
            
                <?php
        //include_once $theme_path . '/templates/include/sidebar.tpl.php'; 
        print render($title_prefix);
        ?>
        <?php if ($page): ?>
            <h1<?php print $title_attributes; ?> class="heading11"><?php print $title; ?></h1>
        <?php endif; ?>
        
        <div class="row art-desc-sec">
            <div class="col-md-12 col-sm-12 col-xs-12 main-content shortcodes"<?php print $content_attributes; ?>>
                <?php print $messages; ?>
                <?php print render($page['content']); ?>
                <?php
                if (!empty($page['sidebar_first']))
                    print render($page['sidebar_first']);
                ?>
            </div>

        </div>
            
            </div>
        </div>
        
    </main>
    <?php
    $theme_path = drupal_get_path('theme', $GLOBALS['theme']);
    include_once $theme_path . '/templates/include/footer.tpl.php';
    ?>

</main>