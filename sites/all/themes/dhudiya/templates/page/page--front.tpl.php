<?php $theme_path = drupal_get_path('theme', $GLOBALS['theme']);
global $base_url; ?>
<script>
    var themePath = '<?php echo $theme_path; ?>';
    var base_url = '<?php echo $base_url; ?>';
</script>
<div class="page" id="top">
    <!-- Home Section -->
     <?php if ($page['home_slider']): ?>
       <?php print render($page['home_slider']); ?>
   <?php endif; ?>    
    <?php include_once $theme_path . '/templates/include/header.tpl.php'; ?>


    <?php if ($page['home_featured']): ?>
       <?php print render($page['home_featured']); ?>
   <?php endif; ?>
   <?php if ($page['home_videos']): ?>
       <?php print render($page['home_videos']); ?>
   <?php endif; ?>
   <?php if ($page['home_news']): ?>
       <?php print render($page['home_news']); ?>
   <?php endif; ?>
   <?php if ($page['home_subscribe']): ?>
       <?php print render($page['home_subscribe']); ?>
   <?php endif; ?>
    <!-- Foter -->
    <?php include_once $theme_path . '/templates/include/footer.tpl.php'; ?>
    <!-- End Foter -->
</div>
<!-- End Page Wrap -->