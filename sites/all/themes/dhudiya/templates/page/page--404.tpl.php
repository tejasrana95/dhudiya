<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @ingroup themeable
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 */
$theme_path = drupal_get_path('theme', $GLOBALS['theme']);
global $base_url;
?>
<script>
    var themePath = '<?php echo $theme_path; ?>';
    var base_url = '<?php echo $base_url; ?>';
</script>
<div id="page-wrapper">
    <div id="page" class="error-page-404">

        <?php if ($page['alert']): ?>
            <?php print render($page['alert']); ?>

        <?php endif; ?>

        <?php include_once $theme_path . '/templates/include/header.tpl.php'; ?>



        <div id="content" class="column">
            <div class="section container">
                <?php if ($page['help']): ?>
                    <div id="help" class="container">
                        <?php print render($page['help']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['notice']): ?>
                    <div id="notice" class="container">
                        <?php print render($page['notice']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($messages): ?>
                    <div id="messages">
                        <div class="section container clearfix">
                            <?php print $messages; ?>
                        </div>
                    </div> <!-- /.section, /#messages -->
                <?php endif; ?>
                <?php if ($page['highlighted']): ?>
                    <div id="highlighted">
                        <?php print render($page['highlighted']); ?>
                    </div>
                <?php endif; ?>
                <a id="main-content"></a>
                <?php print render($title_prefix); ?>
                
                <?php print render($title_suffix); ?>
                <?php if ($breadcrumb): ?>
                    <div id="breadcrumb"><?php print $breadcrumb; ?></div>
                <?php endif; ?>
                <?php if ($action_links): ?>
                    <ul class="action-links">
                        <?php print render($action_links); ?>
                    </ul>
                <?php endif; ?>
                <?php if ($page['page_top_one']): ?>
                    <div id="page_top_one" class="">
                        <?php print render($page['page_top_one']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['page_top_second']): ?>
                    <div id="page_top_second" class="">
                        <?php print render($page['page_top_second']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['page_top_third']): ?>
                    <div id="page_top_third" class="">
                        <?php print render($page['page_top_third']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['page_top_fourth']): ?>
                    <div id="page_top_fourth" class="">
                        <?php print render($page['page_top_fourth']); ?>
                    </div>
                <?php endif; ?>
            </div>

            <?php if ($page['free_space_top']): ?>
                <div id="free_space_top" class="">
                    <?php print render($page['free_space_top']); ?>
                </div>
            <?php endif; ?>

            <div class="container">
                <?php if ($page['featured']): ?>
                    <div id="featured" class="container">
                        <?php print render($page['featured']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($title): ?>
                    <h1 class="title" id="page-title">404</h1>
                       <h2> <?php print $title; ?></h2>
                    
                <?php endif; ?>
                <?php print render($page['content']); ?>
                <div class="error-homepage-link"><a href="/" title="Click to go hompeage">Home</a> | <a href="/search/google" title="Click to go search">Search</a></div>
                <?php print $feed_icons; ?>
            </div>
            <?php if ($page['free_space_bottom']): ?>
                <div id="free_space_bottom" class="">
                    <?php print render($page['free_space_bottom']); ?>
                </div>
            <?php endif; ?>
            <div class="container">
                <?php if ($page['page_bottom_one']): ?>
                    <div id="page_bottom_one" class="">
                        <?php print render($page['page_bottom_one']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['page_bottom_second']): ?>
                    <div id="page_bottom_second" class="">
                        <?php print render($page['page_bottom_second']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['page_bottom_third']): ?>            
                    <div id="page_bottom_third" class="">
                        <?php print render($page['page_bottom_third']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['page_bottom_fourth']): ?>
                    <div id="page_bottom_fourth" class="" >
                        <?php print render($page['page_bottom_fourth']); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div> <!-- /.section, /#content -->
    <?php include_once $theme_path . '/templates/include/footer.tpl.php'; ?>
</div>
