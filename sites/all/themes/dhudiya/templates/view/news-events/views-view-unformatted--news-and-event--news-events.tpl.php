<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($rows)): ?>
	<div class="row">
		<?php foreach ($rows as $id => $row): ?>
			<div class="col-md-3 col-sm-12 min-heigh-500">
				<?php print $row; ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>