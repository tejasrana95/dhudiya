<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($rows)): ?>
	
		<?php $i=1; foreach ($rows as $id => $row): ?>
		<?php  if($i==1): ?>
				<div class="row">
			<?php
			 endif;?>
			<div class="col-md-4 col-sm-12">
				<?php print $row; ?>
			</div>
			<?php  if($i==3): ?>
				</div>
				<div class="clearfix">&nbsp;</div>
			<?php
			$i=0;
			 endif; $i++; ?>
		<?php endforeach; ?>
<?php endif; ?>