<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($rows)): ?>
	<ul class="clearlist work-full-slider video-featured">
		<?php foreach ($rows as $id => $row): ?>
			<li>
				<?php print $row; ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>