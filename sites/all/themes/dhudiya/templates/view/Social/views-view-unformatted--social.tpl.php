<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="row masonry">
    <?php foreach ($rows as $id => $row): ?>
        <div class="col-sm-6 col-md-4 col-lg-4 mb-60 mb-xs-40 ">
            <div class="boxs">
            <?php print $row; ?>
        </div>
        </div>
    <?php endforeach; ?>
</div>