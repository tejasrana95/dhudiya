<?php
/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php if ($content): ?>
    <section class="content-section full-width with-col cover custom-padding custom-margin content-section-59293391657dc">
        <div class="container">
            <div class="row vc_row-fluid">
                <div class="wpb_column col-md-12">
                    <div class="section-text text-block " style=""><h4>SUBSCRIBE</h4>
                    </div>
                    <div class="newsletter-wrapper elegant-newsletter">
                        <div class="row">
                            <?php
                            $i = 1;
                            foreach ($contents as $content_part):
                                ?>
                                <div class="<?php print $classes; ?> <?php echo $class; ?>">
                                    <?php
                                    if ($i == 1):
                                        ?>
                                        <div class="col-md-4">
                                            <div class="newsletter-label newsletter-label-style2">
                                                <h5></h5>
                                                <?php print render($content_part); ?>
                                            </div>
                                        </div><!-- /.col-md-4 -->
                                    </div>
                                <?php endif; ?>
                                <?php
                                if ($i == 2):
                                    ?>
                                    <div class="col-md-6 pb-50 right sm-float-none">
                                        <div class=" mt-30 mt-xs-0 mb-10">
                                            <div class="widget_wysija_cont shortcode_wysija">
                                                <div id="msg-form-wysija-shortcode59292bd528ae8-2" class="wysija-msg ajax">

                                                </div>
                                                <?php print render($content_part); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-tip">
                                            <i class="fa fa-info-circle"></i>Don’t worry, we don’t spam. You can opt out any time.
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>