<?php

/**
 * Add body classes if certain regions have content.
 */
function dhudiya_preprocess_html(&$variables) {
    if (!empty($variables['page']['featured'])) {
        $variables['classes_array'][] = 'featured';
    }

    if (!empty($variables['page']['triptych_first']) || !empty($variables['page']['triptych_middle']) || !empty($variables['page']['triptych_last'])) {
        $variables['classes_array'][] = 'triptych';
    }

    if (!empty($variables['page']['footer_firstcolumn']) || !empty($variables['page']['footer_secondcolumn']) || !empty($variables['page']['footer_thirdcolumn']) || !empty($variables['page']['footer_fourthcolumn'])) {
        $variables['classes_array'][] = 'footer-columns';
    }

    // Add conditional stylesheets for IE
    drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
    drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));



//bring current url info
    $url = request_uri();
    $query = drupal_get_query_parameters();

    //url seems to be: "path?field=value" - (encoded) "path%3Ffield%3Dvalue"
    if (strpos($url, '?') !== FALSE and empty($query)) {
        //we'll have $url['path'] = 'path' and $url['query'] = 'field=value'
        $url = parse_url($url);

        //make $query associative array
        parse_str($url['query'], $query);


        //go to where you were supposed to go from the beginning
        if ($url['path'] and is_array($query))
            drupal_goto($url['path'], array('query' => $query));
    }
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function dhudiya_process_html(&$variables) {
    // Hook into color.module.
    if (module_exists('color')) {
        _color_html_alter($variables);
    }
}

/**
 * Override or insert variables into the page template.
 */
function dhudiya_process_page(&$variables) {
    // Hook into color.module.
    if (module_exists('color')) {
        _color_page_alter($variables);
    }
    // Always print the site name and slogan, but if they are toggled off, we'll
    // just hide them visually.
    $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
    $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
    if ($variables['hide_site_name']) {
        // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
        $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
    }
    if ($variables['hide_site_slogan']) {
        // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
        $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
    }
    // Since the title and the shortcut link are both block level elements,
    // positioning them next to each other is much simpler with a wrapper div.
    if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
        // Add a wrapper div using the title_prefix and title_suffix render elements.
        $variables['title_prefix']['shortcut_wrapper'] = array(
            '#markup' => '<div class="shortcut-wrapper clearfix">',
            '#weight' => 100,
        );
        $variables['title_suffix']['shortcut_wrapper'] = array(
            '#markup' => '</div>',
            '#weight' => -99,
        );
        // Make sure the shortcut link is the first item in title_suffix.
        $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
    }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function dhudiya_preprocess_maintenance_page(&$variables) {
    // By default, site_name is set to Drupal if no db connection is available
    // or during site installation. Setting site_name to an empty string makes
    // the site and update pages look cleaner.
    // @see template_preprocess_maintenance_page
    if (!$variables['db_is_active']) {
        $variables['site_name'] = '';
    }
    drupal_add_css(drupal_get_path('theme', 'dhudiya') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function dhudiya_process_maintenance_page(&$variables) {
    // Always print the site name and slogan, but if they are toggled off, we'll
    // just hide them visually.
    $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
    $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
    if ($variables['hide_site_name']) {
        // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
        $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
    }
    if ($variables['hide_site_slogan']) {
        // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
        $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
    }
}

/**
 * Override or insert variables into the node template.
 */
function dhudiya_preprocess_node(&$variables) {
    if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
        $variables['classes_array'][] = 'node-full';
    }
}

/**
 * Override or insert variables into the block template.
 */
function dhudiya_preprocess_block(&$variables) {
    // In the header region visually hide block titles.
    if ($variables['block']->region == 'header') {
        $variables['title_attributes_array']['class'][] = 'element-invisible';
    }
}

/**
 * Implements theme_menu_tree().
 */

/**
 * Implements theme_field__field_type().
 */
function dhudiya_field__taxonomy_term_reference($variables) {
    $output = '';

    // Render the label, if it's not hidden.
    if (!$variables['label_hidden']) {
        $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
    }

    // Render the items.
    $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
    foreach ($variables['items'] as $delta => $item) {
        $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
    }
    $output .= '</ul>';

    // Render the top-level DIV.
    $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] . '>' . $output . '</div>';

    return $output;
}

function dhudiya_preprocess_page(&$vars, $hook) {
    if (isset($vars['node']->type)) {
        $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
    }
}

function dhudiya_menu_link(array $variables) {
    $element = $variables['element'];
    $sub_menu = '';
    $sub_menu = '';
    /* added for overview page */
    $sub_menu_parent = '';
    $element['#attributes']['class'][] = 'menu-item menu-item-object-page menu-item-' . $variables['element']['#original_link']['mlid'];
    if ($element['#below'] && !empty($element['#below'])) {
        // this link has children
        if (isset($element['#attributes']['class']) && !empty($element['#attributes']['class'])) {
            array_push($element['#attributes']['class'], 'menu-item-has-children');
        } else {
            $element['#attributes']['class'] = 'menu-item menu-item-object-page menu-item-' . $variables['element']['#original_link']['mlid'];
        }
    }
    if ($element['#below']) {
        if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] >= 1)) {

            unset($element['#below']['#theme_wrappers']);
            $element['#title'] .= ' <i class="mn-angle-icon fa fa-angle-down"></i>';
            //  $element['#attributes']['class'][] = 'dropdown';
            $element['#localized_options']['html'] = TRUE;

            $sub_menu = '<ul class="mn-sub">' . $sub_menu_parent . drupal_render($element['#below']) . '</ul>';

            $element['#localized_options']['attributes']['data-target'] = $element['#href'];
            $element['#localized_options']['attributes']['class'][] = 'mn-has-sub';
            $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
        }
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function dhudiya_menu_tree($variables) {

    return '<ul id="primary-nav" class="clearlist scroll-nav local-scroll">' . $variables['tree'] . '</ul>';
}

function debugs($array, $die = true, $dump = false) {
    if (!$dump) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    } else {
        var_dump($array);
    }
    if ($die) {
        die();
    }
}

function target_gen($attributes) {
    if (isset($attributes['target']) && $attributes['target'] == '_blank') {
        return 'target="_blank"';
    }
    return '';
}

function dhudiya_preprocess_region(&$variables) {
    // Create the $content variable that templates expect.
    if ($variables['region'] == "home_subscribe") {
        //debugs($variables['elements']);
        $variables['contents'] = $variables['elements'];
    }
    $variables['content'] = $variables['elements']['#children'];
    $variables['region'] = $variables['elements']['#region'];
    $count = count(element_children($variables['elements']));
    $variables['count'] = $count;
    $variables['classes_array'][] = drupal_region_class($variables['region']);
    $variables['theme_hook_suggestions'][] = 'region__' . $variables['region'];
}
