
<div class="footer-social-links mb-30">
    <?php if (isset($items['facebook'])) { ?>
        <a href="<?php echo $items['facebook']; ?>" target="_blank">
            <i class=" fa fa-facebook"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['twitter'])) { ?>
        <a href="<?php echo $items['twitter']; ?>" target="_blank">
            <i class=" fa fa-twitter"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['instagram'])) { ?>
        <a href="<?php echo $items['instagram']; ?>" target="_blank">
            <i class=" fa fa-instagram"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['youtube'])) { ?>
        <a href="<?php echo $items['youtube']; ?>" target="_blank">
            <i class=" fa fa-youtube"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['google_plus'])) { ?>
        <a href="<?php echo $items['google_plus']; ?>" target="_blank">
            <i class=" fa fa-google-plus"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['linkedin'])) { ?>
        <a href="<?php echo $items['linkedin']; ?>" target="_blank">
            <i class=" fa fa-linkedin"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['pinterest'])) { ?>
        <a href="<?php echo $items['pinterest']; ?>" target="_blank">
            <i class=" fa fa-pinterest"></i>
        </a>
    <?php } ?>
    <?php if (isset($items['email'])) { ?>
        <a href="mailto:<?php echo $items['email']; ?>" target="_blank">
            <i class=" fa fa-envelope"></i>
        </a>
    <?php } ?>
</div>


