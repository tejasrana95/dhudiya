<ul class="infi-socila-links">
    <li class="nav-social-icons">
        <?php if (isset($items['facebook'])) { ?>
            <a href="<?php echo $items['facebook']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Facebook">
                    <i class="fa fa-facebook"></i>
                </span>
            </a>
        <?php } ?>
        <?php if (isset($items['twitter'])) { ?>
            <a href="<?php echo $items['twitter']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Twitter">
                    <i class="fa fa-twitter"></i>
                </span>
            </a>
        <?php } ?>
        <?php if (isset($items['youtube'])) { ?>
            <a href="<?php echo $items['youtube']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Youtube">
                    <i class="fa fa-youtube"></i>
                </span>
            </a>
        <?php } ?>
        <?php if (isset($items['instagram'])) { ?>
            <a href="<?php echo $items['instagram']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Instagram">
                    <i class="fa fa-instagram"></i>
                </span>
            </a>
        <?php } ?>
        <?php if (isset($items['linkedin'])) { ?>
            <a href="<?php echo $items['linkedin']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Linkedin">
                    <i class="fa fa-linkedin"></i>
                </span>
            </a>
        <?php } ?>
        <?php if (isset($items['google_plus'])) { ?>
            <a href="<?php echo $items['google_plus']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Google Plus">
                    <i class="fa fa-google-plus"></i>
                </span>
            </a>
        <?php } ?>
        <?php if (isset($items['pinterest'])) { ?>
            <a href="<?php echo $items['pinterest']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Pinterest">
                    <i class="fa fa-pinterest"></i>
                </span>
            </a>
            <?php } ?>
         <?php if (isset($items['email'])) { ?>
            <a href="mailto:<?php echo $items['email']; ?>" target="_blank">
                <span class="mn-soc-link tooltip-bot" title="" data-original-title="Email">
                    <i class="fa fa-envelope"></i>
                </span>
            </a>
            <?php } ?>
    </li>
</ul>