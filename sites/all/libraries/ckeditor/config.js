/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.toolbar = [{ name: 'insert', items: ['Image', 'Youtube']}];
config.youtube_width = '640';
config.youtube_height = '480';
config.youtube_related = false;
config.youtube_privacy = true;
};
